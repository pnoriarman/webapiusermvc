﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;
using Interface;
using WebApiUser_MVC_TEV.Models;
using WebApiUser_MVC_TEV.Controllers;

namespace UnitTestWebApiUser.Tests
{
    [TestClass]
    public class UnitTestWebApiUser
    {
        [TestMethod]
        public void GetUser()
        {
            var mockRepository = new Mock<IUserData>();
            mockRepository.Setup(x => x.GetUser(3)).Returns(new User { ID = 3 });

            var controller = new UserController(mockRepository.Object);

            IHttpActionResult actionResult = controller.GetUser(3);
            var contentResult = actionResult as OkNegotiatedContentResult<User>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.ID);
        }

        [TestMethod]
        public void CreateUser()
        {
            User user = new User()
            {
                Name = "Maria",
                LastName = "Camacho",
                Address = "Venezuela",
                CreateDate = DateTime.Today,
                UpdateDate = DateTime.Today
            };

            var mockRepository = new Mock<IUserData>();
            var controller = new UserController(mockRepository.Object);

            IHttpActionResult actionResult = controller.Create(user);
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<User>;

            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
        }

        [TestMethod]
        public void UpdateUser()
        {
            User user = new User()
            {
                ID = 1,
                Name = "Pastora",
                LastName = "Lopez",
                Address = "Caracas Venezuela",
                UpdateDate = DateTime.Today
            };

            var mockRepository = new Mock<IUserData>();
            mockRepository.Setup(x => x.UpdateUser(1, user)).Returns(user);

            var controller = new UserController(mockRepository.Object);

            IHttpActionResult actionResult = controller.Update(1, user);
            var contentResult = actionResult as OkNegotiatedContentResult<User>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.ID);
        }

        [TestMethod]
        public void DeleteUser()
        {
            var mockRepository = new Mock<IUserData>();
            var controller = new UserController(mockRepository.Object);

            IHttpActionResult actionResult = controller.Delete(5);

            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }
    }
}